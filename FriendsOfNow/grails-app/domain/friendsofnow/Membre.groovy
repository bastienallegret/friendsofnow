package friendsofnow


class Membre {
//	static hasMany = [ photos : PhotoTagee ]
	
	//s�curit�
	String username
	String passwordHash
	ShiroRole role
	static hasMany = [ permissions: String, ecoles: School, etudes: StudyClassOfMembre, evenement: Event]
	
	
	String nom, prenom, email, sexe
	Date datenaissance
	String toString() { return nom+" "+prenom }
    static constraints = {
		permissions minSize:1
		ecoles minSize:0
		etudes minSize:0
		evenement minSize:0
		
		username(blank:false, nullable: false)
		username validator: {
			return it.length() > 6
		}
		passwordHash blank: false
		nom(blank: false)
		prenom(blank: false)
		email(blank: false, email: true)
		datenaissance max: (new Date()-(13*365))
		sexe inList : ["M", "F"]
}
}
