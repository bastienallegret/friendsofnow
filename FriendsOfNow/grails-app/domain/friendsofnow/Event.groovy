package friendsofnow

class Event {
	
	static hasMany = [ Attending : Membre, MaybeAttending : Membre, NotAttending : Membre, invited : Membre ]
	static belongsTo = Membre 

    static constraints = {
    }
}
