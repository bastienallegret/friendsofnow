package friendsofnow



class Activite {

    static hasMany = [ participants : Membre]
	String titre, description
	String toString() { return titre+" - "+description}
    static constraints = {
		titre blank: false
		description blank: false
		participants min: 1
}
}
