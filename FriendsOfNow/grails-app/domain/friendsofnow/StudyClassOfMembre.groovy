package friendsofnow

class StudyClassOfMembre {
	
	Membre participant
	StudyClass classe

    static constraints = {
		classe nullable:false
		participant nullable:false
    }
}
