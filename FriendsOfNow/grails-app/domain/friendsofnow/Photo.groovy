package friendsofnow


class Photo {

//	static hasMany = [ photoT : PhotoTagee ]
   String nomPhoto
	byte[] data

    static constraints = {
		data maxSize: 1000000
		nomPhoto blank: false
    }

}
