



<div class="fieldcontain ${hasErrors(bean: photoInstance, field: 'data', 'error')} required">
	<label for="data">
		<g:message code="photo.data.label" default="Data" />
		<span class="required-indicator">*</span>
	</label>
	<input type="file" id="data" name="data" />
</div>

<div class="fieldcontain ${hasErrors(bean: photoInstance, field: 'nomPhoto', 'error')} required">
	<label for="nomPhoto">
		<g:message code="photo.nomPhoto.label" default="Nom Photo" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="nomPhoto" required="" value="${photoInstance?.nomPhoto}"/>
</div>

