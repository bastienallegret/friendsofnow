<%@ page import="friendsofnow.StudyClass" %>



<div class="fieldcontain ${hasErrors(bean: studyClassInstance, field: 'inscription', 'error')} ">
	<label for="inscription">
		<g:message code="studyClass.inscription.label" default="Inscription" />
		
	</label>
	
<ul class="one-to-many">
<g:each in="${studyClassInstance?.inscription?}" var="i">
    <li><g:link controller="studyClassOfMembre" action="show" id="${i.id}">${i?.encodeAsHTML()}</g:link></li>
</g:each>
<li class="add">
<g:link controller="studyClassOfMembre" action="create" params="['studyClass.id': studyClassInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'studyClassOfMembre.label', default: 'StudyClassOfMembre')])}</g:link>
</li>
</ul>

</div>

