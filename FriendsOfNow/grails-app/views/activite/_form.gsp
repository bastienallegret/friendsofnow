



<div class="fieldcontain ${hasErrors(bean: activiteInstance, field: 'titre', 'error')} required">
	<label for="titre">
		<g:message code="activite.titre.label" default="Titre" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="titre" required="" value="${activiteInstance?.titre}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: activiteInstance, field: 'description', 'error')} required">
	<label for="description">
		<g:message code="activite.description.label" default="Description" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="description" required="" value="${activiteInstance?.description}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: activiteInstance, field: 'participants', 'error')} ">
	<label for="participants">
		<g:message code="activite.participants.label" default="Participants" />
		
	</label>
	<g:select name="participants" from="${Membre.list()}" multiple="multiple" optionKey="id" size="5" value="${activiteInstance?.participants*.id}" class="many-to-many"/>
</div>

