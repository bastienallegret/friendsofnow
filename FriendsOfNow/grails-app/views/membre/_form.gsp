



<div class="fieldcontain ${hasErrors(bean: membreInstance, field: 'username', 'error')} required">
	<label for="username">
		<g:message code="membre.username.label" default="Username" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="username" required="" value="${membreInstance?.username}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: membreInstance, field: 'passwordHash', 'error')} required">
	<label for="passwordHash">
		<g:message code="membre.passwordHash.label" default="Password" />
		<span class="required-indicator">*</span>
	</label>
	<g:passwordField name="password" required="" value=""/>
</div>

<div class="fieldcontain ${hasErrors(bean: membreInstance, field: 'nom', 'error')} required">
	<label for="nom">
		<g:message code="membre.nom.label" default="Nom" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="nom" required="" value="${membreInstance?.nom}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: membreInstance, field: 'prenom', 'error')} required">
	<label for="prenom">
		<g:message code="membre.prenom.label" default="Prenom" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="prenom" required="" value="${membreInstance?.prenom}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: membreInstance, field: 'email', 'error')} required">
	<label for="email">
		<g:message code="membre.email.label" default="Email" />
		<span class="required-indicator">*</span>
	</label>
	<g:field type="email" name="email" required="" value="${membreInstance?.email}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: membreInstance, field: 'datenaissance', 'error')} required">
	<label for="datenaissance">
		<g:message code="membre.datenaissance.label" default="Datenaissance" />
		<span class="required-indicator">*</span>
	</label>
	<g:datePicker name="datenaissance" precision="day"  value="${membreInstance?.datenaissance}"  />
</div>

<div class="fieldcontain ${hasErrors(bean: membreInstance, field: 'sexe', 'error')} ">
	<label for="sexe">
		<g:message code="membre.sexe.label" default="Sexe" />
		
	</label>
	<g:select name="sexe" from="${membreInstance.constraints.sexe.inList}" value="${membreInstance?.sexe}" valueMessagePrefix="membre.sexe" noSelection="['': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: membreInstance, field: 'permissions', 'error')} ">
	<label for="permissions">
		<g:message code="membre.permissions.label" default="Permissions" />
		
	</label>
	
</div>

<div class="fieldcontain ${hasErrors(bean: membreInstance, field: 'roles', 'error')} ">
	<label for="roles">
		<g:message code="membre.roles.label" default="Roles" />
		
	</label>
	<g:select name="roles" from="${ShiroRole.list()}" multiple="multiple" optionKey="id" size="5" value="${membreInstance?.roles*.id}" class="many-to-many"/>
</div>

