package friendsofnow

import org.springframework.dao.DataIntegrityViolationException

class StudyClassController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [studyClassInstanceList: StudyClass.list(params), studyClassInstanceTotal: StudyClass.count()]
    }

    def create() {
        [studyClassInstance: new StudyClass(params)]
    }

    def save() {
        def studyClassInstance = new StudyClass(params)
        if (!studyClassInstance.save(flush: true)) {
            render(view: "create", model: [studyClassInstance: studyClassInstance])
            return
        }

		flash.message = message(code: 'default.created.message', args: [message(code: 'studyClass.label', default: 'StudyClass'), studyClassInstance.id])
        redirect(action: "show", id: studyClassInstance.id)
    }

    def show() {
        def studyClassInstance = StudyClass.get(params.id)
        if (!studyClassInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'studyClass.label', default: 'StudyClass'), params.id])
            redirect(action: "list")
            return
        }

        [studyClassInstance: studyClassInstance]
    }

    def edit() {
        def studyClassInstance = StudyClass.get(params.id)
        if (!studyClassInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'studyClass.label', default: 'StudyClass'), params.id])
            redirect(action: "list")
            return
        }

        [studyClassInstance: studyClassInstance]
    }

    def update() {
        def studyClassInstance = StudyClass.get(params.id)
        if (!studyClassInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'studyClass.label', default: 'StudyClass'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (studyClassInstance.version > version) {
                studyClassInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'studyClass.label', default: 'StudyClass')] as Object[],
                          "Another user has updated this StudyClass while you were editing")
                render(view: "edit", model: [studyClassInstance: studyClassInstance])
                return
            }
        }

        studyClassInstance.properties = params

        if (!studyClassInstance.save(flush: true)) {
            render(view: "edit", model: [studyClassInstance: studyClassInstance])
            return
        }

		flash.message = message(code: 'default.updated.message', args: [message(code: 'studyClass.label', default: 'StudyClass'), studyClassInstance.id])
        redirect(action: "show", id: studyClassInstance.id)
    }

    def delete() {
        def studyClassInstance = StudyClass.get(params.id)
        if (!studyClassInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'studyClass.label', default: 'StudyClass'), params.id])
            redirect(action: "list")
            return
        }

        try {
            studyClassInstance.delete(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'studyClass.label', default: 'StudyClass'), params.id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'studyClass.label', default: 'StudyClass'), params.id])
            redirect(action: "show", id: params.id)
        }
    }
}
