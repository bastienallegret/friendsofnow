import org.apache.shiro.crypto.hash.Sha256Hash

import friendsofnow.Membre;

class BootStrap {

    def init = { servletContext ->
		
		def user = new Membre(username:"user",passwordHash: new Sha256Hash("password").toHex(),nom:"nom1",prenom:"prenom1",email:"nom.prenom@gmail.com",sexe:"M",datenaissance: (new Date())-15*365,permissions:"*:*");
//		def user = new ShiroUser(username:"user",passwordHash: new Sha256Hash("password").toHex(),permissions:"*:*")
		
		user.save(failOnError:true)
    }
    def destroy = {
    }
}
