package friendsofnow



import org.junit.*
import grails.test.mixin.*

@TestFor(StudyClassController)
@Mock(StudyClass)
class StudyClassControllerTests {


    def populateValidParams(params) {
      assert params != null
      // TODO: Populate valid properties like...
      //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/studyClass/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.studyClassInstanceList.size() == 0
        assert model.studyClassInstanceTotal == 0
    }

    void testCreate() {
       def model = controller.create()

       assert model.studyClassInstance != null
    }

    void testSave() {
        controller.save()

        assert model.studyClassInstance != null
        assert view == '/studyClass/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/studyClass/show/1'
        assert controller.flash.message != null
        assert StudyClass.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/studyClass/list'


        populateValidParams(params)
        def studyClass = new StudyClass(params)

        assert studyClass.save() != null

        params.id = studyClass.id

        def model = controller.show()

        assert model.studyClassInstance == studyClass
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/studyClass/list'


        populateValidParams(params)
        def studyClass = new StudyClass(params)

        assert studyClass.save() != null

        params.id = studyClass.id

        def model = controller.edit()

        assert model.studyClassInstance == studyClass
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/studyClass/list'

        response.reset()


        populateValidParams(params)
        def studyClass = new StudyClass(params)

        assert studyClass.save() != null

        // test invalid parameters in update
        params.id = studyClass.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/studyClass/edit"
        assert model.studyClassInstance != null

        studyClass.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/studyClass/show/$studyClass.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        studyClass.clearErrors()

        populateValidParams(params)
        params.id = studyClass.id
        params.version = -1
        controller.update()

        assert view == "/studyClass/edit"
        assert model.studyClassInstance != null
        assert model.studyClassInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/studyClass/list'

        response.reset()

        populateValidParams(params)
        def studyClass = new StudyClass(params)

        assert studyClass.save() != null
        assert StudyClass.count() == 1

        params.id = studyClass.id

        controller.delete()

        assert StudyClass.count() == 0
        assert StudyClass.get(studyClass.id) == null
        assert response.redirectedUrl == '/studyClass/list'
    }
}
